package com.shumov.tm;

import java.util.HashMap;
import java.util.Map;

public enum Command {

    CREATE_TASK("create-task", "Create new task"),
    CREATE_PROJECT("create-project", "Create new project"),

    EDIT_TASK_NAME_BY_ID("edit-task-name", "Edit task name"),
    EDIT_PROJECT_NAME_BY_ID("edit-project-name", "Edit project name"),

    REMOVE_TASK_BY_ID("remove-task", "Remove selected task"),
    REMOVE_PROJECT_BY_ID("remove-project", "Remove selected project" ),

    TASK_LIST("task-list", "Show all tasks"),
    PROJECT_LIST("project-list", "Show all projects"),

    ADD_TASK_IN_PROJECT("add-task", "Add task in project"),

    REVIEW_PROJECT_TASKS("review-project-tasks", "Review project tasks"),

    HELP("help", "Show all commands"),
    EXIT("exit", "Exit");



    private String commandText;
    private String description;
    private static Map<String, Command> Commands = new HashMap<>();


    // Инициализациия map добавлением всех доступных комманд (Всех объектов Enum)
    static {
        for (Command command : Command.values()){
            Commands.put(command.commandText, command);
        }
    }

    Command(String title, String description){
        this.commandText = title;
        this.description = description;
    }

    public String getCommandText() {
        return commandText;
    }

    public String getDescription() {
        return description;
    }

    public static Map<String, Command> getCommands() {
        return Commands;
    }
}
