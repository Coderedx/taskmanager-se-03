package com.shumov.tm;

public class Main
{
    public static void main( String[] args )
    {
        System.out.println("WELCOME TO TASK MANAGER");
        System.out.println("ENTER \"help\" TO GET COMMAND LIST");
        Controller controller = Controller.getInstance();
        while (true){
            controller.execute();
        }
    }
}
