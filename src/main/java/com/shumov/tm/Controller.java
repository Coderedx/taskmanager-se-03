package com.shumov.tm;

import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {

    private static Controller controller = new Controller();

    private Controller(){

    }
    private Scanner scanner = new Scanner(System.in);

    public static Controller getInstance(){
        return controller;
    }

    // Создание нового проекта и задания
    private void createTask() {
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        String taskName = scanner.nextLine();
        if (taskName.isEmpty()){
            System.out.println("WRONG TASK NAME!");
            return;
        }
        Task.getList().add(new Task(taskName));
    }

    private void createProject() {
        System.out.println("[PROJECT CREATE]\nENTER PROJECT NAME:");
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()){
            System.out.println("WRONG PROJECT NAME!");
            return;
        }
        Project.getList().add(new Project(projectName));
    }


    // Отображение всех проектов и заданий из списков
    private void showTaskList() {
        System.out.println("[TASK LIST]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
        }
        for (Task task : Task.getList()) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }

    private void showTaskList(List<Task> tasks) {
        System.out.println("[TASK LIST]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
        }
        for (Task task : tasks) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }





    private void showProjectList() {
        System.out.println("[PROJECT LIST]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
        }
        for (Project project : Project.getList()) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }




    // Изменить имя по ID
    private void editTaskNameById() {
        System.out.println("[EDIT TASK NAME BY ID]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER TASK ID:");
        String strTaskId = scanner.nextLine();
        if (strTaskId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        String taskName = scanner.nextLine();
        if (taskName.isEmpty()){
            System.out.println("WRONG TASK NAME!");
            return;
        }
        for (Task task : Task.getList()) {
            if (strTaskId.equals(task.getId())) {
                task.setName(taskName);
                System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("TASK WITH THIS ID DOES NOT EXIST!");
    }



    private void editProjectNameById() {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()){
            System.out.println("WRONG PROJECT NAME!");
            return;
        }
        for (Project project : Project.getList()) {
            if (strProjectId.equals(project.getId())) {
                project.setName(projectName);
                System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");

    }


    // Удаление по ID
    private void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER TASK ID:");
        String strTaskId = scanner.nextLine();
        if(strTaskId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        for (Task task : Task.getList()) {
            if (strTaskId.equals(task.getId())) {
                Task.getList().remove(task);
                System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("TASK WITH THIS ID DOES NOT EXIST!");
    }

    private void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        for (Project project : Project.getList()) {
            if (strProjectId.equals(project.getId())) {
//              03. Удаление задач из проекта при удалении проекта:
                List<Task> tasks = getProjectTasks(strProjectId);
                if (tasks!=null && !tasks.isEmpty()){
                    Task.getList().removeAll(tasks);
                }
                Project.getList().remove(project);
                System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");
    }




    // 03. Добавление задачи в проект:

    private void addTaskInProject(){
        System.out.println("[ADD TASK IN PROJECT]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()){
            System.out.println("WRONG PROJECT ID!");
            return;
        } else if(getProjectById(strProjectId)==null) {
            System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");
            return;
        }
        System.out.println("ENTER TASK ID:");
        String strTaskId = scanner.nextLine();
        if (strTaskId.isEmpty()){
            System.out.println("WRONG TASK ID!");
            return;
        }
        Task task = getTaskById(strTaskId);
        if(task==null){
            System.out.println("TASK WITH THIS ID DOES NOT EXIST!");
            return;
        } else {
            task.setProjectId(strProjectId);
            System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
        }
    }




    // Просмотр всех задач в проекте по id проекта:
    private void reviewProjectTasks(){
        List<Task> tasks = getProjectTasks();
        if(tasks==null || tasks.isEmpty()){
            System.out.println("NO PROJECT TASKS");
            return;
        } else {
            showTaskList(tasks);
        }
    }

    private List<Task> getProjectTasks() {
        System.out.println("[REVIEW PROJECT TASKS]");
        if (Project.getList().isEmpty()) {
            System.out.println("PROJECT LIST IS EMPTY!");
            return null;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()) {
            System.out.println("WRONG PROJECT ID!");
            return null;
        } else if (getProjectById(strProjectId) == null) {
            System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");
            return null;
        }
        List<Task> tasks = new ArrayList<>();
        for (Task task : Task.getList()) {
            if (strProjectId.equals(task.getProjectId())) {
                tasks.add(task);
            }
        }
        if (tasks.isEmpty()) {
            return null;
        }
        return tasks;
    }

    //Реализация метода для внутренней работы(Не для консоли)
    private List<Task> getProjectTasks(String strProjectId) {
        if (Project.getList().isEmpty()) {
            return null;
        }
        if (strProjectId==null || strProjectId.isEmpty()) {
            return null;
        } else if (getProjectById(strProjectId) == null) {
            return null;
        }
        List<Task> tasks = new ArrayList<>();
        for (Task task : Task.getList()) {
            if (strProjectId.equals(task.getProjectId())) {
                tasks.add(task);
            }
        }
        if (tasks.isEmpty()) {
            return null;
        }
        return tasks;
    }


    private Project getProjectById(String id){
        for (Project project: Project.getList()) {
            if(id.equals(project.getId())){
                return project;
            }
        }
        return null;
    }

    private Task getTaskById(String id){
        for (Task task: Task.getList()) {
            if(id.equals(task.getId())){
                return task;
            }
        }
        return null;
    }

    // help
    private void help() {
        getAllCommands();
    }

    private void exit() {
        System.exit(0);
    }

    //Список команд
    private void getAllCommands() {
        for (Command command : Command.values()) {
            System.out.println(command.getCommandText()+": "+command.getDescription());
        }
    }

    /** Основное выполнение программы:
     *  1. Проверка ввёденной команды разбита на несколько этапов
     */

    public void execute() {
        System.out.println("\nENTER COMMAND:");
        Command command = Command.getCommands().get(scanner.nextLine().toLowerCase());
        commandSwitchStep1(command);
        commandSwitchStep2(command);
    }

    private void commandSwitchStep1(Command command) {
        if (command != null) {
            switch (command) {
                case HELP:
                    this.help();
                    break;
                case EXIT:
                    this.exit();
                    break;
                case CREATE_TASK:
                    this.createTask();
                    break;
                case CREATE_PROJECT:
                    this.createProject();
                    break;
                case REMOVE_TASK_BY_ID:
                    this.removeTaskById();
                    break;
                case REMOVE_PROJECT_BY_ID:
                    this.removeProjectById();
                    break;
            }
        }
    }

    private void commandSwitchStep2(Command command) {
        if (command != null) {
            switch (command) {
                case EDIT_TASK_NAME_BY_ID:
                    this.editTaskNameById();
                    break;
                case EDIT_PROJECT_NAME_BY_ID:
                    this.editProjectNameById();
                    break;
                case TASK_LIST:
                    this.showTaskList();
                    break;
                case PROJECT_LIST:
                    this.showProjectList();
                    break;
                case ADD_TASK_IN_PROJECT:
                    this.addTaskInProject();
                    break;
                case REVIEW_PROJECT_TASKS:
                    this.reviewProjectTasks();
                    break;
            }
        } else {
            System.out.println("WRONG COMMAND!");
        }
    }
}